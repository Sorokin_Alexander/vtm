program main
  	implicit none
  	real(8), ALLOCATABLE :: R(:, :), P(:, :), A(:, :), Q(:, :)
  	integer m, n
	real(8) ans, t1, t2

  	external dnrm2

    interface

		subroutine BD(R, m, n, P, A, Q)
			real(8), intent(in) :: R(:, :)
			real(8), intent(out) :: P(:, :), A(:, :), Q(:, :)
			integer, intent(in) :: m, n
		end subroutine

		real(8) function checkAns(R, m, n, P, A, Q)
			real(8), intent(in) :: R(:, :), P(:, :), A(:, :), Q(:, :)
			integer, intent(in) :: m, n
		end function

		real(8) function norm(matrix, m, n)
			real(8), dimension(:, :), intent(in) :: matrix
			integer, intent(in) :: m, n
		end function

    end interface
    
    print*, 'Enter matrix size'
    read(*, *) m
    n = m

    allocate(R(m, n), P(m, m), A(m, n), Q(n, n));
	
	call random_number(R)

	call cpu_time(t1)
	call BD(R, m, n, P, A, Q)
	call cpu_time(t2)

	print*, 'Working time: ', t2 - t1


	ans = checkAns(R, m, n, P, A, Q)
	print*, 'Error: ', ans
	print*, 'Abs error: ', ans / norm(R, m, n) 

	deallocate(R, P, A, Q)
end




real(8) function sgn(x)
    real(8), intent(in) :: x
    if (x .eq. 0) then
        sgn = 0
    else
        sgn = sign(real(1, 8), x)
    end if
end function

subroutine BD(R, m, n, P, A, Q)
    real(8), intent(in) :: R(:, :)
    real(8), intent(out) :: P(:, :), A(:, :), Q(:, :)
	integer, intent(in) :: m, n
    real(8), allocatable :: u(:), v(:), u1(:), v1(:)
	real(8) dnrm2

    integer i, j
	
	external dnrm2, dgemv, dger

    interface

        real(8) function sgn(x)
            real(8), intent(in) :: x
        end function

    end interface

    A = R
    P = 0
    Q = 0

    allocate(u(m), v(n), u1(n), v1(m))

    do i = 1, m
        P(i, i) = 1
    end do

    do i = 1, n
        Q(i, i) = 1
    end do

    do i = 1, n
    	u(1:m - i + 1) = A(i:m, i);
    	u(1) = u(1) + sgn(u(1)) * dnrm2(m - i + 1, u, 1)
		u(1:m - i + 1) = u(1:m - i + 1) / dnrm2(m - i + 1, u, 1)
		call dgemv('t', m - i + 1, n, 1.D0, a(i, 1) , m, u, 1, 0.D0, u1, 1)   ! u1 = A'u
		call dger(m - i + 1, n, -2.D0, u, 1, u1, 1, a(i, 1), m)               ! A = A - 2u(u1)' = A - 2u(A'u)' = A - 2uu'A = (I - 2uu')A
		call dgemv('n', m, m - i + 1, 1.D0, p(1, i), m, u, 1, 0.D0, v1, 1)
		call dger(m, m -i + 1, -2.D0, v1, 1, u, 1, p(1, i), m) 

	if (i <= n - 2) then
		v(1:n - i) = A(i, i + 1 : n)
		v(1) = v(1) + sgn(v(1)) * dnrm2(n - i, v, 1)
		v(1:n - i) = v(1:n - i) / dnrm2(n - i, v, 1)
		call dgemv('n', m, n - i, 1.D0, a(1, i + 1), m, v, 1, 0.D0, v1, 1)
		call dger(m, n - i, -2.D0, v1, 1, v, 1, a(1, i + 1), m)
		call dgemv('t', n - i, n, 1.D0, q(i + 1, 1), n, v, 1, 0.D0, u1, 1)
		call dger(n - i, n, -2.D0, v, 1, u1, 1, q(i + 1, 1), n)
	end if	
    end do

	deallocate(u, v, u1, v1)
end subroutine

real(8) function checkAns(R, m, n, P, A, Q)
	real(8), intent(in) :: R(:, :), P(:, :), A(:, :), Q(:, :)
	integer, intent(in) :: m, n
	real(8), allocatable :: L(:, :), U(:, :)
	
	external dgemm
	interface
		real(8) function norm(matrix, m, n)
			real(8), dimension(:, :), intent(in) :: matrix
			integer, intent(in) :: m, n
		end function
	end interface

	allocate(L(m, n), U(m, n))
	
	L = R
	call dgemm('n', 'n', m, n, m, 1.D0, P, m, A, m, 0.D0, U, m)
	call dgemm('n', 'n', m, n, n, -1.D0, U, m, Q, n, 1.D0, L, m)
	
	checkAns = norm(L, m, n)
	deallocate(L, U)
end function

real(8) function norm(matrix, m, n)
    real(8), dimension(:, :), intent(in) :: matrix
	integer, intent(in) :: m, n
    integer i, j

    norm = 0;
    do i = 1, m
    do j = 1, n
        norm = norm + matrix(i, j) * matrix(i, j)
    end do
    end do
    norm = sqrt(norm)
end function

