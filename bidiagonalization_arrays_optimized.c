#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define sign(a) ( ( (a) < 0 )  ?  -1   : ( (a) > 0 ) )

double norm(const double *vector, size_t n, size_t k);
void leftHouseholder(double *matrix, size_t m, size_t n, const double *vector, size_t k);
void rightHouseholder(double *matrix, size_t m, size_t n, const double *vector, size_t k);

double **BiDiag(double *matrix, size_t m, size_t n);

double matrixNorm(double **matrix, size_t m, size_t n);
double **matMul(double **first, double **second, size_t m, size_t k, size_t n);
double **matSub(double **first, double **second, size_t m, size_t n);

int main(int argc, char *argv[])
{
    size_t n = atoi(argv[2]); // n columns
    size_t m = atoi(argv[1]); // m rows

    srand(time(NULL));

    double *matrix = malloc(sizeof(double) * m * n);
    for(int i = 0; i < m; i++)
    {
        for(int j = 0; j < n; j++)
            matrix[i * n + j] = (double) rand() / RAND_MAX;

    }

    clock_t time;

    time = clock();

    double **ans = BiDiag(matrix, m, n);

    time = clock() - time;

    printf("Bidiagonalization \"time\": %llu\n", (unsigned long long) time);

    free(matrix);
    free(ans[0]);
    free(ans[1]);
    free(ans[2]);
    free(ans);

    return 0;
}

double norm(const double *vector, size_t n, size_t k)
{
    double sum = 0;
    for(size_t i = k; i < n; i++)
        sum += vector[i] * vector[i];
    return sqrt(sum);
}

void leftHouseholder(double *matrix, size_t m, size_t n, const double *vector, size_t k) // A(k:m, :) = A(k:m, :) - 2 * u * (u' * A(k:m, :))
{
    double *tmp = calloc(n, sizeof(double));        // for u'A

    for(size_t i = 0; i < n; i++)                   // u'A
        for(size_t j = k; j < m; j++)
            tmp[i] += matrix[j * n + i] * vector[j];

    for(size_t i = k; i < m; i++)                   // A - u*u'*A
        for(size_t j = 0; j < n; j++)
            matrix[i * n + j] -= 2*vector[i]*tmp[j];

    free(tmp);
}

void rightHouseholder(double *matrix, size_t m, size_t n, const double *vector, size_t k) // A(:, k:n) = A(:, k:n) - 2 * (A(:, k:n) * v) * v'
{
    double *tmp = calloc(m, sizeof(double));        // for Av

    for(size_t i = 0; i < m; i++)                   // Av
        for(size_t j = k; j < n; j++)
            tmp[i] += matrix[i * n + j] * vector[j];

    for(size_t i = 0; i < m; i++)                   // A - Avv'
        for(size_t j = k; j < n; j++)
            matrix[i * n + j] -= 2 * tmp[i] * vector[j];

    free(tmp);
}

double **BiDiag(double *matrix, size_t m, size_t n)
{
    double *u = calloc(m, sizeof(double));
    double *v = calloc(n, sizeof(double));

    double *A = calloc(m * n, sizeof(double));
    double *P = calloc(m * m, sizeof(double));
    double *Q = calloc(n * n, sizeof(double));

    for(size_t i = 0; i < m; i++)
    {
        memcpy(A + i * n, matrix + i * n, n * sizeof(double));
    }

    for(size_t i = 0; i < m; i++)
    {
        P[i * m + i] = 1;
    }

    for(size_t i = 0; i < n; i++)
    {
        Q[i * n + i] = 1;
    }

    double tmp_norm;

    for(size_t k = 0; k < n; k++)
    {
        for(size_t i_c = k; i_c < m; i_c++)
            u[i_c] = A[i_c * n + k];

        tmp_norm = norm(u, m, k);
        u[k] += sign(u[k]) * tmp_norm;

        tmp_norm = norm(u, m, k);
        for(size_t i_c = k; i_c < m; i_c++)
            u[i_c] /= tmp_norm;

        leftHouseholder(A, m, n, u, k);
        rightHouseholder(P, m, m, u, k);


        if (k < n - 2)
        {
            for(size_t i_c = k + 1; i_c < n; i_c++)
                v[i_c] = A[k * n + i_c];

            tmp_norm = norm(v, n, k + 1);
            v[k + 1] += sign(v[k + 1]) * tmp_norm;

            tmp_norm = norm(v, n, k + 1);
            for(size_t i_c = k + 1; i_c < n; i_c++)
                v[i_c] /= tmp_norm;

            rightHouseholder(A, m, n, v, k + 1);
            leftHouseholder(Q, n, n, v, k + 1);
       }
    }

    free(u);
    free(v);

    double **ans = malloc(3 * sizeof(double **));
    ans[0] = P;
    ans[1] = A;
    ans[2] = Q;
    return ans;
}
