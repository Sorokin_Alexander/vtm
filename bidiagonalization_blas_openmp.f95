program main
  	implicit none
  	real(8), ALLOCATABLE :: R(:, :), P(:, :), A(:, :), Q(:, :)
  	integer m, n
	real(8) ans, t1, t2, omp_get_wtime

  	external dnrm2

    interface

		subroutine BD(R, m, n, P, A, Q)
			real(8), intent(in) :: R(:, :)
			real(8), intent(out) :: P(:, :), A(:, :), Q(:, :)
			integer, intent(in) :: m, n
		end subroutine

		real(8) function checkAns(R, m, n, P, A, Q)
			real(8), intent(in) :: R(:, :), P(:, :), A(:, :), Q(:, :)
			integer, intent(in) :: m, n
		end function

		real(8) function norm(matrix, m, n)
			real(8), dimension(:, :), intent(in) :: matrix
			integer, intent(in) :: m, n
		end function

    end interface
    
    print*, 'Enter matrix size'
    read(*, *) m
    n = m

    allocate(R(m, n), P(m, m), A(m, n), Q(n, n));
	
	call random_number(R)

	!call cpu_time(t1)
	t1 = omp_get_wtime();
	call BD(R, m, n, P, A, Q)
	t2 = omp_get_wtime();

	print*, 'Working time: ', t2 - t1


	ans = checkAns(R, m, n, P, A, Q)
	print*, 'Error: ', ans
	print*, 'Abs error: ', ans / norm(R, m, n) 

	deallocate(R, P, A, Q)
end




real(8) function sgn(x)
    real(8), intent(in) :: x
    if (x .eq. 0) then
        sgn = 0
    else
        sgn = sign(real(1, 8), x)
    end if
end function

subroutine BD(R, m, n, P, A, Q)
    real(8), intent(in) :: R(:, :)
    real(8), intent(out) :: P(:, :), A(:, :), Q(:, :)
	integer, intent(in) :: m, n
    real(8), allocatable :: u(:), v(:), u1(:), v1(:)
	real(8) dnrm2, ddot
	real(8) dotp

    integer i, j
	
	external dnrm2, dgemv, dger

    interface

        real(8) function sgn(x)
            real(8), intent(in) :: x
        end function
        
        subroutine leftUpdate(A, m, n, u, k)
            integer, intent(in) :: m, n, k
            real(8), intent(in) :: A(:, :), u(:)
        end subroutine
        
        subroutine rightUpdate(A, m, n, u, k)
            integer, intent(in) :: m, n, k
            real(8), intent(in) :: A(:, :), u(:)
        end subroutine

    end interface

    A = R
    P = 0
    Q = 0

    allocate(u(m), v(n), u1(n), v1(m))

    do i = 1, m
        P(i, i) = 1
    end do

    do i = 1, n
        Q(i, i) = 1
    end do

    do i = 1, n
    	u(1:m - i + 1) = A(i:m, i);
    	u(1) = u(1) + sgn(u(1)) * dnrm2(m - i + 1, u, 1)
		u(1:m - i + 1) = u(1:m - i + 1) / dnrm2(m - i + 1, u, 1)
		!do j = i, n                                                        !  Нужно было начинать с 1 но там нули
		    !dotp = -2 * ddot(m - i + 1, u, 1, a(i, j), 1)
		    !call daxpy(m - i + 1, dotp, u, 1, a(i, j), 1)
		!end do
		call leftUpdate(A, m, n, u, i)
		call rightUpdate(P, m, m, u, i)
		!do j = 1, m
		    !dotp = -2 * ddot(m - i + 1, u, 1, p(j, i), n)
		    !call daxpy(m - i + 1, dotp, u, 1, p(j, i), n)
		!end do 

	if (i <= n - 2) then
		v(1:n - i) = A(i, i + 1 : n)
		v(1) = v(1) + sgn(v(1)) * dnrm2(n - i, v, 1)
		v(1:n - i) = v(1:n - i) / dnrm2(n - i, v, 1)
		call rightUpdate(A, m, n, v, i + 1)
		!do j = 1, n                                                        ! А тут так не прокатит
		    !dotp = -2 * ddot(n - i, v, 1, Q(i + 1, j), 1)
		    !call daxpy(n - i, dotp, v, 1, Q(i + 1, j), 1)
	    !end do
	    call leftUpdate(Q, n, n, v, i + 1)
	end if	
    end do

	deallocate(u, v, u1, v1)
end subroutine

real(8) function checkAns(R, m, n, P, A, Q)
	real(8), intent(in) :: R(:, :), P(:, :), A(:, :), Q(:, :)
	integer, intent(in) :: m, n
	real(8), allocatable :: L(:, :), U(:, :)
	
	external dgemm
	interface
		real(8) function norm(matrix, m, n)
			real(8), dimension(:, :), intent(in) :: matrix
			integer, intent(in) :: m, n
		end function
	end interface

	allocate(L(m, n), U(m, n))
	
	L = R
	call dgemm('n', 'n', m, n, m, 1.D0, P, m, A, m, 0.D0, U, m)
	call dgemm('n', 'n', m, n, n, -1.D0, U, m, Q, n, 1.D0, L, m)
	
	checkAns = norm(L, m, n)
	deallocate(L, U)
end function

real(8) function norm(matrix, m, n)
    real(8), dimension(:, :), intent(in) :: matrix
	integer, intent(in) :: m, n
    integer i, j

    norm = 0;
    do i = 1, m
    do j = 1, n
        norm = norm + matrix(i, j) * matrix(i, j)
    end do
    end do
    norm = sqrt(norm)
end function

subroutine leftUpdate(A, m, n, u, k)
    integer, intent(in) :: m, n, k
    real(8), intent(in) :: A(:, :), u(:)
    integer i
    real(8) dotp, ddot
    
    !$OMP PARALLEL DO PRIVATE(dotp)
    do i = 1, n
        dotp = -2 * ddot(m - k + 1, u, 1, a(k, i), 1)
		call daxpy(m - k + 1, dotp, u, 1, a(k, i), 1)
    end do
    !$OMP END PARALLEL DO
end subroutine

subroutine rightUpdate(A, m, n, u, k)
    integer, intent(in) :: m, n, k
    real(8), intent(in) :: A(:, :), u(:)
    integer i
    real(8) dotp, ddot
    
    !$OMP PARALLEL DO PRIVATE(dotp)
    do i = 1, m
        dotp = -2 * ddot(m - k + 1, u, 1, a(i, k), n)
		call daxpy(m - k + 1, dotp, u, 1, a(i, k), n)
	end do
	!$OMP END PARALLEL DO
end subroutine


