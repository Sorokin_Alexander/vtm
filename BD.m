function [P A Q] = BD(R)
[m, n] = size(R);
P = eye(m);
Q = eye(n);
A = R;

for i = 1:n
u = A(i:m, i);
u(1) = u(1) + sign(u(1)) * norm(u, 'fro');
u = u / norm(u, 'fro');
A(i:end, :) = A(i:end, :) - 2*u*u'*A(i:end, :);
P(:, i:end) = P(:, i:end) - 2*P(:, i:end)*u*u';

if (i <= n - 2)
v = A(i, i + 1 : n);
v(1) = v(1) + sign(v(1)) * norm(v, 'fro');
v = v / norm(v, 'fro');
A(:, i + 1:end) = A(:, i + 1:end) - 2 * A(:, i + 1:end) * v' * v;
Q(i + 1:end, :) = Q(i + 1:end, :) - 2 * v' * v * Q(i + 1:end, :);
endif

end


end
