#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <omp.h>

#define sign(a) ( ( (a) < 0 )  ?  -1   : ( (a) > 0 ) )

double norm(const double *vector, size_t n, size_t k);
void leftHouseholder(double **matrix, size_t m, size_t n, const double *vector, size_t k);
void rightHouseholder(double **matrix, size_t m, size_t n, const double *vector, size_t k);

double ***BiDiag(double **matrix, size_t m, size_t n);

double matrixNorm(double **matrix, size_t m, size_t n);
double **matMul(double **first, double **second, size_t m, size_t k, size_t n);
double **matSub(double **first, double **second, size_t m, size_t n);

int main(int argc, char *argv[])
{
    size_t n = atoi(argv[2]); // n columns
    size_t m = atoi(argv[1]); // m rows


    double **matrix = malloc(sizeof(double *) * m);
    for(int i = 0; i < m; i++)
    {
        matrix[i] = malloc(sizeof(double) * n);
        for(int j = 0; j < n; j++)
            matrix[i][j] = (double) rand() / RAND_MAX;

    }

    double wtime;

    wtime = omp_get_wtime();

    double ***ans = BiDiag(matrix, m, n);

    wtime = omp_get_wtime() - wtime;

    printf("Bidiagonalization \"time\" %ld\n",  wtime);



    //double **tmp1 = matMul(ans[0], ans[1], m, m, n);
    //double **tmp2 = matMul(tmp1, ans[2], m, n, n);
    //double **tmp3 = matSub(matrix, tmp2, m, n);

    //printf("%.15lf\n", matrixNorm(tmp3, m, n) / matrixNorm(matrix, m, n));

    for(size_t i = 0; i < m; i++)
    {
        //free(tmp1[i]);
        //free(tmp2[i]);
        //free(tmp3[i]);
        free(matrix[i]);
        free(ans[0][i]);
        free(ans[1][i]);
    }

    for(size_t i = 0; i < n; i++)
    {
        free(ans[2][i]);
    }

    //free(tmp1);
    //free(tmp2);
    //free(tmp3);
    free(matrix);
    free(ans[0]);
    free(ans[1]);
    free(ans[2]);
    free(ans);

    return 0;
}

double norm(const double *vector, size_t n, size_t k)
{
    double sum = 0;
    for(size_t i = k; i < n; i++)
        sum += vector[i] * vector[i];
    return sqrt(sum);
}

void leftHouseholder(double **matrix, size_t m, size_t n, const double *vector, size_t k) // A(k:m, :) = A(k:m, :) - 2 * u * (u' * A(k:m, :))
{
    double *tmp = calloc(n, sizeof(double));        // for u'A

    for(size_t i = 0; i < n; i++)                   // u'A
        for(size_t j = k; j < m; j++)
            tmp[i] += matrix[j][i] * vector[j];

    for(size_t i = k; i < m; i++)                   // A - u*u'*A
        for(size_t j = 0; j < n; j++)
            matrix[i][j] -= 2*vector[i]*tmp[j];

    free(tmp);
}

void rightHouseholder(double **matrix, size_t m, size_t n, const double *vector, size_t k) // A(:, k:n) = A(:, k:n) - 2 * (A(:, k:n) * v) * v'
{
    double *tmp = calloc(m, sizeof(double));        // for Av

    for(size_t i = 0; i < m; i++)                   // Av
        for(size_t j = k; j < n; j++)
            tmp[i] += matrix[i][j] * vector[j];

    for(size_t i = 0; i < m; i++)                   // A - Avv'
        for(size_t j = k; j < n; j++)
            matrix[i][j] -= 2 * tmp[i] * vector[j];

    free(tmp);
}

double ***BiDiag(double **matrix, size_t m, size_t n)
{
    double *u = calloc(m, sizeof(double));
    double *v = calloc(n, sizeof(double));

    double **A = malloc(m * sizeof(double *));
    double **P = malloc(m * sizeof(double *));
    double **Q = malloc(n * sizeof(double *));

    for(size_t i = 0; i < m; i++)
    {
        A[i] = malloc(n * sizeof(double));
        memcpy(A[i], matrix[i], n * sizeof(double));
    }

    for(size_t i = 0; i < m; i++)
    {
        P[i] = calloc(m, sizeof(double));
        P[i][i] = 1;
    }

    for(size_t i = 0; i < n; i++)
    {
        Q[i] = calloc(n, sizeof(double));
        Q[i][i] = 1;
    }

    double tmp_norm;

    for(size_t k = 0; k < n; k++)
    {
        for(size_t i_c = k; i_c < m; i_c++)
            u[i_c] = A[i_c][k];

        tmp_norm = norm(u, m, k);
        u[k] += sign(u[k]) * tmp_norm;

        tmp_norm = norm(u, m, k);
        for(size_t i_c = k; i_c < m; i_c++)
            u[i_c] /= tmp_norm;

        leftHouseholder(A, m, n, u, k);
        rightHouseholder(P, m, m, u, k);


        if (k < n - 2)
        {
            for(size_t i_c = k + 1; i_c < n; i_c++)
                v[i_c] = A[k][i_c];

            tmp_norm = norm(v, n, k + 1);
            v[k + 1] += sign(v[k + 1]) * tmp_norm;

            tmp_norm = norm(v, n, k + 1);
            for(size_t i_c = k + 1; i_c < n; i_c++)
                v[i_c] /= tmp_norm;

            rightHouseholder(A, m, n, v, k + 1);
            leftHouseholder(Q, n, n, v, k + 1);
       }
    }

    free(u);
    free(v);

    double ***ans = malloc(3 * sizeof(double **));
    ans[0] = P;
    ans[1] = A;
    ans[2] = Q;
    return ans;
}

double matrixNorm(double **matrix, size_t m, size_t n)
{
    double sum = 0;
    for(size_t i = 0; i < m; i++)
        for(size_t j = 0; j < n; j++)
            sum += matrix[i][j] * matrix[i][j];
    return sqrt(sum);
}

double **matMul(double **first, double **second, size_t m, size_t k, size_t n)
{
    unsigned long long number = m * n;
    double **res = malloc(m * sizeof(double *));
    for(size_t i = 0; i < m; i++)
        res[i] = calloc(n, sizeof(double));

    for(size_t i = 0; i < m; i++)
        for(size_t j = 0; j < n; j++)
            for(size_t h = 0; h < k; h++)
                res[i][j] += first[i][h] * second[h][j];

    return res;

}

double **matSub(double **first, double **second, size_t m, size_t n)
{
    double **res = malloc(m * sizeof(double *));
    for(size_t i = 0; i < m; i++)
        res[i] = calloc(n, sizeof(double));

    for(size_t i = 0; i < m; i++)
        for(size_t j = 0; j < n; j++)
            res[i][j] = first[i][j] - second[i][j];
    return res;
}
