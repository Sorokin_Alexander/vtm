#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#define BLOCK_SIZE 2


// (b, p) -> c : (p + num - b) % num
// (c, p) -> b : (p + num - c) % num

void gen_matrix(double *matrix, int size);
void print_matrix(double *matrix, int size);

void vec_mat_mul(double *vec, double *mat, double *res, int size);
void mat_vec_mul(double *vec, double *mat, double *res, int size);

int main(int argc, char *argv[])
{
    MPI_Init(&argc, &argv);

    int proc_number, cur_proc;
    MPI_Comm_size(MPI_COMM_WORLD, &proc_number);
    MPI_Comm_rank(MPI_COMM_WORLD, &cur_proc);
    MPI_Status status;

    double *full_matrix = NULL;   // A = PBQ
    double *full_p_matrix = NULL;
    double *full_q_matrix = NULL;
    double *full_b_matrix = NULL;

    int matrix_size = BLOCK_SIZE * proc_number;

    double *blocks = malloc(sizeof(double *) * proc_number * BLOCK_SIZE * BLOCK_SIZE);


    //Initialize and send matrix
    if (cur_proc == 0)
    {
        full_matrix = malloc(sizeof(double) * matrix_size * matrix_size);
        //full_p_matrix = calloc(matrix_size * matrix_size, sizeof(double));
        //full_q_matrix = calloc(matrix_size * matrix_size, sizeof(double));
        full_b_matrix = calloc(matrix_size * matrix_size, sizeof(double));

        gen_matrix(full_matrix, matrix_size);

        printf("FULL MATRIX: \n");
        print_matrix(full_matrix, matrix_size);
        printf("\n\n\n");

        double *buf = malloc(sizeof(double) * BLOCK_SIZE * BLOCK_SIZE);

        for(int i = 0; i < proc_number * proc_number; i++)
        {
            // FORMING BLOCK                                    //      0 1 2       0 1 2
            int bi = i / proc_number;  //row                    //      3 4 5   =>  1 2 0
            int bj = i % proc_number;  //col                    //      6 7 8       2 0 1

            for(int j = 0; j < BLOCK_SIZE; j++)
                memcpy(buf + j * BLOCK_SIZE, full_matrix + (bj * BLOCK_SIZE + j) * matrix_size + bi * BLOCK_SIZE, sizeof(double) * BLOCK_SIZE);


            // SEND BLOCK
            int rec_proc = (i + bi) % proc_number;

            if (rec_proc != 0)
                MPI_Send(buf, BLOCK_SIZE * BLOCK_SIZE, MPI_DOUBLE, (i + bi) % proc_number, 0, MPI_COMM_WORLD);
            else
                memcpy(blocks + bi * BLOCK_SIZE * BLOCK_SIZE, buf, BLOCK_SIZE * BLOCK_SIZE * sizeof(double));
        }

        free(buf);
    }
    else
    {
        for(int i = 0; i < proc_number; i++)
            MPI_Recv(blocks + i * BLOCK_SIZE * BLOCK_SIZE, BLOCK_SIZE * BLOCK_SIZE, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
    }

//    for(int i = 0; i < proc_number; i++)
//    {
//        if (i == cur_proc)
//        {
//            printf("%d proc's blocks: \n", cur_proc);
//            for(int j = 0; j < proc_number; j++)
//                print_matrix(blocks + j * BLOCK_SIZE * BLOCK_SIZE, BLOCK_SIZE);
//            printf("\n\n\n");
//
//            sleep(1);
//        }
//        MPI_Barrier(MPI_COMM_WORLD);
//    }

    double *reflect    = malloc(sizeof(double) * BLOCK_SIZE * proc_number);
    double *tmp_res     = malloc(sizeof(double) * BLOCK_SIZE * proc_number);
    double *reflected   = malloc(sizeof(double) * BLOCK_SIZE * proc_number);
    double *part_ref    = malloc(sizeof(double) * BLOCK_SIZE);
    double part_norm, norm;

    int *displs = malloc(sizeof(int) * proc_number);
    int *recvcounts = malloc(sizeof(int) * proc_number);

    for(int i = 0; i < proc_number; i++)
        recvcounts[i] = BLOCK_SIZE;

    for(int i = 0; i < proc_number * BLOCK_SIZE; i++)
    {
        int bj = i / BLOCK_SIZE; // Столбцовый номер блока
        int s  = i % BLOCK_SIZE; // Номер столбца внутри малого блока
        int block_num = (cur_proc + proc_number - bj) % proc_number; // Номер блока внутри процессора, который содержит кусочек столбца

        // чтобы правильно собрать вектор отражения нужно для каждого процессора установить смещение посылаемых им данных
        // например, нужный столбец лежит в во втором блоке нулевого процессора
        // т.е. в исходной матрице он лежал во второй блочной строке
        // (потому что номера блоков внутри процессора равны номерам блочных строк исходной матрицы из которых был взят этот блок)
        // но если отправить его просто так - в векторе отражения он будет стоять в блочной строке равной номеру процесса
        // поэтому необходимо определить его смещение
        int block_displ = block_num * BLOCK_SIZE;

        // Собираем все смещения
        MPI_Allgather(&block_displ, 1, MPI_INT, displs, 1, MPI_INT, MPI_COMM_WORLD);


        // Необходимо обнулить элементы которые уже не должны участвовать в отражении
        // Номер блока внутри процессора == строковому номеру блока в исходной матрице
        if (bj > block_num)
        {
            // Столбцовый номер больше чем строчный - обнулить все
            memset(part_ref, 0, sizeof(double) * BLOCK_SIZE);
            part_norm = 0;
        }
        else if (bj == block_num)
        {
            // Стобцовый номер совпадет со строчным - необходимо обнулить часть элементов
            memset(part_ref, 0, sizeof(double) * s);
            memcpy(part_ref + s, blocks + block_num * BLOCK_SIZE * BLOCK_SIZE + s * BLOCK_SIZE + s, sizeof(double) * (BLOCK_SIZE - s));

            part_norm = 0;
            for(int k = s; k < BLOCK_SIZE; k++)
                part_norm += part_ref[k] * part_ref[k];
        }
        else
        {
            // Столбцовый номер меньше строкового - ничего не нужно обнулять
            memcpy(part_ref, blocks + block_num * BLOCK_SIZE * BLOCK_SIZE + s * BLOCK_SIZE, sizeof(double) * BLOCK_SIZE);
            part_norm = 0;

            for(int k = 0; k < BLOCK_SIZE; k++)
                part_norm += part_ref[k] * part_ref[k];
        }




        // Собираем вектор отражения
        MPI_Allgatherv(part_ref, BLOCK_SIZE, MPI_DOUBLE, reflect, recvcounts, displs, MPI_DOUBLE, MPI_COMM_WORLD);

        // Собираем норму
        MPI_Allreduce(&part_norm, &norm, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);




        // Вычисляем вектор отражения
        double tmp = reflect[i];
        reflect[i] += copysign(1.0, reflect[i]) * sqrt(norm);
        norm = 2 * norm + 2 * copysign(1.0, reflect[i]) * tmp * sqrt(norm);
        norm = sqrt(norm);

        for(int k = i; k < matrix_size; k++)
            reflect[k] /= norm;




        // Умножаем блоки на строку отражения
        for(int k = 0; k < proc_number; k++)
            vec_mat_mul(reflect + k * BLOCK_SIZE, blocks + k * BLOCK_SIZE * BLOCK_SIZE, tmp_res + k * BLOCK_SIZE, BLOCK_SIZE);

        // Собераем строчку после умножения
        // Номер блочной строчки совпадает с block_num(bj)
        for(int k = 0; k < proc_number; k++)
        {
            int block_num = (cur_proc + proc_number - k) % proc_number;
            MPI_Allreduce(tmp_res + block_num * BLOCK_SIZE, reflected + k * BLOCK_SIZE, BLOCK_SIZE, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        }

        // Обновляем матрицу
        // Пробежимся по блокам
        for(int k = 0; k < proc_number; k++)
        {
            int col_num = (cur_proc + proc_number - k) % proc_number;
            for(int bj = 0; bj < BLOCK_SIZE; bj++)
                for(int bi = 0; bi < BLOCK_SIZE; bi++)
                    blocks[k * BLOCK_SIZE * BLOCK_SIZE + bj * BLOCK_SIZE + bi] -= 2 * reflect[k * BLOCK_SIZE + bi] * reflected[col_num * BLOCK_SIZE + bj];
        }



//        if (cur_proc == 0)
//        {
//            printf("reflect vector: \n");
//            for(int j = 0; j < proc_number * BLOCK_SIZE; j++)
//                printf("%lf ", reflect[j]);
//            printf("\n");
//
//            printf("vector norm: %lf\n", norm);
//        }
//        MPI_Barrier(MPI_COMM_WORLD);




    if (i < matrix_size - 2)
    {
        int bi = i / BLOCK_SIZE; // Строковый номер блока == Номер блока
        int s = i % BLOCK_SIZE; // Номер строки внутри блока

        block_num = (cur_proc + proc_number - bi) % proc_number; // Столбцовый номер блока

        block_displ = block_num * BLOCK_SIZE;

        // Собираем все смещения
        MPI_Allgather(&block_displ, 1, MPI_INT, displs, 1, MPI_INT, MPI_COMM_WORLD);


        // Необходимо обнулить элементы которые уже не должны участвовать в отражении
        if (bi > block_num)
        {
            memset(part_ref, 0, sizeof(double) * BLOCK_SIZE);
            part_norm = 0;
        }
        else if (bi == block_num)
        {
            memset(part_ref, 0, sizeof(double) * (s + 1));

            for(int k = s + 1; k < BLOCK_SIZE; k++)
                part_ref[k] = blocks[bi * BLOCK_SIZE * BLOCK_SIZE + k * BLOCK_SIZE + s];

            part_norm = 0;
            for(int k = s + 1; k < BLOCK_SIZE; k++)
                part_norm += part_ref[k] * part_ref[k];
        }
        else
        {
            for(int k = 0; k < BLOCK_SIZE; k++)
                part_ref[k] = blocks[bi * BLOCK_SIZE * BLOCK_SIZE + k * BLOCK_SIZE + s];
            part_norm = 0;

            for(int k = 0; k < BLOCK_SIZE; k++)
                part_norm += part_ref[k] * part_ref[k];
        }

         // Собираем вектор отражения
        MPI_Allgatherv(part_ref, BLOCK_SIZE, MPI_DOUBLE, reflect, recvcounts, displs, MPI_DOUBLE, MPI_COMM_WORLD);

        // Собираем норму
        MPI_Allreduce(&part_norm, &norm, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);


        // Вычисляем вектор отражения
        double tmp = reflect[i+1];
        reflect[i+1] += copysign(1.0, reflect[i+1]) * sqrt(norm);
        norm = 2 * norm + 2 * copysign(1.0, reflect[i+1]) * tmp * sqrt(norm);
        norm = sqrt(norm);

        for(int k = i + 1; k < matrix_size; k++)
            reflect[k] /= norm;


        // Умножаем блоки на строку отражения
        // Тут получилось удобно для редуцирования
        for(int k = 0; k < proc_number; k++)
        {
            int tmp_s = (cur_proc + proc_number - k) % proc_number;
            mat_vec_mul(reflect + tmp_s * BLOCK_SIZE, blocks + k * BLOCK_SIZE * BLOCK_SIZE, tmp_res + k * BLOCK_SIZE, BLOCK_SIZE);
        }

        MPI_Allreduce(tmp_res, reflected, matrix_size, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);


        // Обновляем матрицу
        // Пробежимся по блокам
        for(int k = 0; k < proc_number; k++)
        {
            int col_num = (cur_proc + proc_number - k) % proc_number;
            for(int bj = 0; bj < BLOCK_SIZE; bj++)
                for(int bi = 0; bi < BLOCK_SIZE; bi++)
                    blocks[k * BLOCK_SIZE * BLOCK_SIZE + bj * BLOCK_SIZE + bi] -= 2 * reflected[k * BLOCK_SIZE + bi] * reflect[col_num * BLOCK_SIZE + bj];
        }


//        if (cur_proc == 0)
//        {
//            printf("reflect vector: \n");
//            for(int j = 0; j < proc_number * BLOCK_SIZE; j++)
//                printf("%lf ", tmp_res[j]);
//            printf("\n");
//
//            printf("vector norm: %lf\n", norm);
//        }
//        MPI_Barrier(MPI_COMM_WORLD);

    }

    }


    // Собираем результирующую матрицу
    for(int i = 0; i < matrix_size; i++)
    {
        int bj = i / BLOCK_SIZE; // Столбцовый номер блока
        int s  = i % BLOCK_SIZE; // Номер столбца внутри малого блока
        int block_num = (cur_proc + proc_number - bj) % proc_number; // Номер блока внутри процессора, который содержит кусочек столбца
        int block_displ = block_num * BLOCK_SIZE;

        // Собираем все смещения
        MPI_Allgather(&block_displ, 1, MPI_INT, displs, 1, MPI_INT, MPI_COMM_WORLD);

        MPI_Gatherv(blocks + block_num * BLOCK_SIZE * BLOCK_SIZE + s * BLOCK_SIZE, BLOCK_SIZE, MPI_DOUBLE, full_b_matrix + i * matrix_size,
                    recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    }

    free(blocks);

    free(reflect);
    free(tmp_res);
    free(part_ref);
    free(reflected);

    free(displs);
    free(recvcounts);

    if (cur_proc == 0)
    {
        printf("matrix: \n");
        print_matrix(full_b_matrix, matrix_size);
        free(full_matrix);
        free(full_b_matrix);
    }

    MPI_Finalize();
    return 0;
}

void gen_matrix(double *matrix, int size)
{
    srand(time(NULL));
    for(int i = 0; i < size * size; i++)
        matrix[i] = (double) rand() / (double) RAND_MAX;

/*
    for(int i = 0; i < size; i++)
        for(int j = 0; j < size; j++)
            matrix[i * size + j] = (j + 1) * 10 + (i + 1);
*/
}

void print_matrix(double *matrix, int size)
{
    for(int j = 0; j < size; j++)
    {
        for(int i = 0; i < size; i++)
            printf("%lf ", matrix[i * size + j]);
        printf("\n");
    }

    printf("\n");
}

void vec_mat_mul(double *vec, double *mat, double *res, int size)
{
    for(int i = 0; i < size; i++)
    {
        res[i] = 0;
        for(int j = 0; j < size; j++)
            res[i] += vec[j] * mat[i * size + j];
    }
}

void mat_vec_mul(double *vec, double *mat, double *res, int size)
{
    for(int i = 0; i < size; i++)
    {
        res[i] = 0;
        for(int j = 0; j < size; j++)
            res[i] += vec[j] * mat[j * size + i];
    }
}
