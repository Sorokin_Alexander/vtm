program main
  	implicit none
  	real(8), ALLOCATABLE :: R(:, :), P(:, :), A(:, :), Q(:, :)
  	integer m, n
	real(8) ans, t1, t2

  	external dnrm2

    interface

		subroutine BD(R, m, n, P, A, Q)
			real(8), intent(in) :: R(:, :)
			real(8), intent(out) :: P(:, :), A(:, :), Q(:, :)
			integer, intent(in) :: m, n
		end subroutine

		real(8) function checkAns(R, m, n, P, A, Q)
			real(8), intent(in) :: R(:, :), P(:, :), A(:, :), Q(:, :)
			integer, intent(in) :: m, n
		end function

		real(8) function norm(matrix, m, n)
			real(8), dimension(:, :), intent(in) :: matrix
			integer, intent(in) :: m, n
		end function

    end interface
    
    print*, 'Enter matrix size'
    read(*, *) m
    n = m

    allocate(R(m, n), P(m, m), A(m, n), Q(n, n));
	
	call random_number(R)

	call cpu_time(t1)
	call BD(R, m, n, P, A, Q)
	call cpu_time(t2)

	print*, 'Working time: ', t2 - t1


	ans = checkAns(R, m, n, P, A, Q)
	print*, 'Error: ', ans
	print*, 'Abs error: ', ans / norm(R, m, n) 

	deallocate(R, P, A, Q)
end


real(8) function norm(matrix, m, n)
    real(8), dimension(:, :), intent(in) :: matrix
	integer, intent(in) :: m, n
    integer i, j

    norm = 0;
    do i = 1, m
    do j = 1, n
        norm = norm + matrix(i, j) * matrix(i, j)
    end do
    end do
    norm = sqrt(norm)
end function

real(8) function sgn(x)
    real(8), intent(in) :: x
    if (x .eq. 0) then
        sgn = 0
    else
        sgn = sign(real(1, 8), x)
    end if
end function

subroutine BD(R, m, n, P, A, Q)
    real(8), intent(in) :: R(:, :)
    real(8), intent(out) :: P(:, :), A(:, :), Q(:, :)
	integer, intent(in) :: m, n
    real(8), allocatable :: u(:, :)
    real(8), allocatable :: v(:, :)
    integer i, j

    interface

        subroutine print2d(matrix)
            real(8), dimension(:, :), intent(in) :: matrix
            integer i, j
        end subroutine

        real(8) function sgn(x)
            real(8), intent(in) :: x
        end function

       real(8) function norm(matrix, m, n)
            real(8), dimension(:, :), intent(in) :: matrix
	        integer, intent(in) :: m, n
        end function

    end interface

    !Preparations

    A = R
    P = 0
    Q = 0

    do i = 1, m
        P(i, i) = 1
    end do

    do i = 1, n
        Q(i, i) = 1
    end do

    !Prepared

    !Main loop

    do i = 1, n
        allocate(u(m - i + 1, 1));
        u(:, 1)  = A(i:m, i);
        u(1, 1) = u(1, 1) + sgn(u(1, 1)) * norm(u, m - i + 1, 1)
        u(:, 1) = u(:, 1) / norm(u, m - i + 1, 1)
        A(i:m, :) = A(i:m, :) - 2 * matmul(u, matmul(transpose(u), A(i:m, :)))
        P(:, i:m) = P(:, i:m) - 2 * matmul(matmul(P(:, i:m), u), transpose(u))
        deallocate(u)

        if (i <= n - 2) then
            allocate(v(1, n - i))
            v(1, :) = A(i, i + 1 : n)
            v(1, 1) = v(1, 1) + sgn(v(1, 1)) * norm(v, 1, n - i)
            v(1, :) = v(1, :) / norm(v, 1, n - i)
            A(:, i + 1 : n) = A(:, i + 1 : n) - 2 * matmul(matmul(A(:, i + 1 : n), transpose(v)), v)
            Q(i + 1 : n, :) = Q(i + 1 : n, :) - 2 * matmul(transpose(v), matmul(v, Q(i + 1 : n, :)))
            deallocate(v)
        end if
    end do


end subroutine

real(8) function checkAns(R, m, n, P, A, Q)
	real(8), intent(in) :: R(:, :), P(:, :), A(:, :), Q(:, :)
	integer, intent(in) :: m, n
	real(8), allocatable :: L(:, :), U(:, :)
	
	external dgemm
	interface
		real(8) function norm(matrix, m, n)
			real(8), dimension(:, :), intent(in) :: matrix
			integer, intent(in) :: m, n
		end function
	end interface

	allocate(L(m, n), U(m, n))
	
	L = R
	call dgemm('n', 'n', m, n, m, 1.D0, P, m, A, m, 0.D0, U, m)
	call dgemm('n', 'n', m, n, n, -1.D0, U, m, Q, n, 1.D0, L, m)
	
	checkAns = norm(L, m, n)
	deallocate(L, U)
end function
